package harsh.com.musicplayer.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Toast;

import com.eftimoff.androipathview.PathView;
import com.google.android.gms.ads.MobileAds;

import butterknife.Bind;
import butterknife.ButterKnife;
import harsh.com.musicplayer.R;

/*Welcome screen activity*/
public class SplashActivity extends AppCompatActivity {

    @Bind(R.id.imgAppIcon)
    PathView imgAppIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        MobileAds.initialize(this, "ca-app-pub-3415371062508470~8865005964");
        /*Asks for permissions*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WAKE_LOCK}, 100);
        }
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);

        /*Setting up the animation, Used the eftimoff library*/
        imgAppIcon.setFillAfter(true);
        imgAppIcon.useNaturalColors();
        imgAppIcon.getPathAnimator()
                .delay(100)
                .duration(2500)
                .interpolator(new AccelerateDecelerateInterpolator())
                .start();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            /*If permissions granted launch the home screen*/
                            startActivity(new Intent(SplashActivity.this, MusicHomeActivity.class));
                            finish();
                        }
                    }, 2500);
                } else {
                    Toast.makeText(SplashActivity.this, R.string.permissions_required, Toast.LENGTH_SHORT).show();
                    ActivityCompat.finishAffinity(this);
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}