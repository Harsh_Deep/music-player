package harsh.com.musicplayer.fragment;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import harsh.com.musicplayer.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutAppFragment extends Fragment {

    @Bind(R.id.txtLibOneHeader)
    TextView txtLibOneHeader;
    @Bind(R.id.txtLibOne)
    TextView txtLibOne;

    @Bind(R.id.txtLibTwoHeader)
    TextView txtLibTwoHeader;
    @Bind(R.id.txtLibTwo)
    TextView txtLibTwo;

    @Bind(R.id.txtLibThreeHeader)
    TextView txtLibThreeHeader;
    @Bind(R.id.txtLibThree)
    TextView txtLibThree;

    @Bind(R.id.txtLibFourHeader)
    TextView txtLibFourHeader;
    @Bind(R.id.txtLibFour)
    TextView txtLibFour;

    @Bind(R.id.txtLibFiveHeader)
    TextView txtLibFiveHeader;
    @Bind(R.id.txtLibFive)
    TextView txtLibFive;

    public AboutAppFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about_app, container, false);
        ButterKnife.bind(this, view);
        populate();
        return view;
    }

    private void populate() {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Whitney-Book-Bas.otf");
        Typeface typefaceHeaders = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Whitney-Semibold-Bas.otf");

        /*GSON*/
        txtLibOneHeader.setTypeface(typefaceHeaders);
        txtLibOne.setTypeface(typeface);
        String gsonLib = "<a href='https://github.com/google/gson'>GSON</a>";
        txtLibOneHeader.setText(Html.fromHtml(gsonLib));
        txtLibOneHeader.setMovementMethod(LinkMovementMethod.getInstance());
        String gsonDesc = "Gson is a Java library that can be used to convert Java Objects into their JSON representation. " +
                "It can also be used to convert a JSON string to an equivalent Java object. " +
                "Gson can work with arbitrary Java objects including pre-existing objects that you do not have source-code of.";
        txtLibOne.setText(gsonDesc);

        /*Pathview*/
        txtLibTwoHeader.setTypeface(typefaceHeaders);
        txtLibTwo.setTypeface(typeface);
        String pathViewLib = "<a href='https://github.com/geftimov/android-pathview'>Pathview</a>";
        txtLibTwoHeader.setText(Html.fromHtml(pathViewLib));
        txtLibTwoHeader.setMovementMethod(LinkMovementMethod.getInstance());
        String pathViewDesc = "This library is used for animating the welcome screen icon. " +
                "Click the name given above for more description and usage in a project";
        txtLibTwo.setText(pathViewDesc);

        /*Butterknife*/
        txtLibThreeHeader.setTypeface(typefaceHeaders);
        txtLibThree.setTypeface(typeface);
        String butterknifeLib = "<a href='http://jakewharton.github.io/butterknife/'>Butter Knife</a>";
        txtLibThreeHeader.setText(Html.fromHtml(butterknifeLib));
        txtLibThreeHeader.setMovementMethod(LinkMovementMethod.getInstance());
        String butterknifeLibDesc = "Field and method binding for Android views which uses annotation processing to generate boilerplate code for you." +
                " For more details click the name given above.";
        txtLibThree.setText(butterknifeLibDesc);

        /*Sliding up panel*/
        txtLibFourHeader.setTypeface(typefaceHeaders);
        txtLibFour.setTypeface(typeface);
        String slidingPanel = "<a href='https://github.com/umano/AndroidSlidingUpPanel'>Sliding Up Panel</a>";
        txtLibFourHeader.setText(Html.fromHtml(slidingPanel));
        txtLibFourHeader.setMovementMethod(LinkMovementMethod.getInstance());
        String slidingPanelDesc = "This library provides a simple way to add a draggable sliding up panel (popularized by Google Music and Google Maps) to your Android application." +
                " Please click on the above name to know more.";
        txtLibFour.setText(slidingPanelDesc);

        /*Glide*/
        txtLibFiveHeader.setTypeface(typefaceHeaders);
        txtLibFive.setTypeface(typeface);
        String glideLib = "<a href='https://github.com/bumptech/glide'>Glide</a>";
        txtLibFiveHeader.setText(Html.fromHtml(glideLib));
        txtLibFiveHeader.setMovementMethod(LinkMovementMethod.getInstance());
        String glideDesc = "Glide is a fast and efficient open source media management and image loading framework for Android that wraps media decoding, " +
                "memory and disk caching, and resource pooling into a simple and easy to use interface. Glide supports fetching, decoding, and displaying video stills, " +
                "images, and animated GIFs. Glide includes a flexible API that allows developers to plug in to almost any network stack. ";
        txtLibFive.setText(glideDesc);
    }
}
