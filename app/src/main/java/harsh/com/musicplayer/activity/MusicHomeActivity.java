package harsh.com.musicplayer.activity;

import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import harsh.com.musicplayer.R;
import harsh.com.musicplayer.dto.SongDTO;
import harsh.com.musicplayer.fragment.AboutAppFragment;
import harsh.com.musicplayer.fragment.AllSongsFragment;
import harsh.com.musicplayer.fragment.FavouritesFragment;
import harsh.com.musicplayer.service.MusicService;
import harsh.com.musicplayer.util.FavouriteManager;

/**
 * Created by Harsh on 9/7/2017.
 */

public class MusicHomeActivity extends AppCompatActivity implements AllSongsFragment.OnHeadlineSelectedListener,
        FavouritesFragment.OnHeadlineSelectedListener, MediaController.MediaPlayerControl, SeekBar.OnSeekBarChangeListener,
        MusicService.ServiceCallbacks {

    // Declaring and initialising variables using Butterknife

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.drawerLayout)
    DrawerLayout drawer_layout;
    @Bind(R.id.navigation_view)
    NavigationView navigation_view;
    @Bind(R.id.imgCompactDisc)
    ImageView imgCompactDisc;
    @Bind(R.id.btnPlay)
    Button btnPlay;
    @Bind(R.id.btnFavourite)
    ImageButton btnFavourite;
    @Bind(R.id.btnPause)
    Button btnPause;
    @Bind(R.id.btnPlayer)
    ImageButton btnPlayer;
    @Bind(R.id.btnNext)
    ImageButton btnNext;
    @Bind(R.id.btnPrevious)
    ImageButton btnPrevious;
    @Bind(R.id.btnShuffle)
    ImageButton btnShuffle;
    @Bind(R.id.btnLoop)
    ImageButton btnLoop;
    @Bind(R.id.txtSongDisplayName)
    TextView txtSongDisplayName;
    @Bind(R.id.songProgressBar)
    SeekBar songProgressBar;
    @Bind(R.id.imgThumbnail)
    ImageView imgThumbnail;
    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @Bind(R.id.sliding_layout)
    SlidingUpPanelLayout nowPlayingLayout;
    Button btnFeelingLucky;

    public static MusicService musicService;
    private Intent playIntent;
    private boolean musicBound = false;
    private boolean playbackPaused = false;
    private MenuItem previousMenuItem;
    private static final String TAG = MusicHomeActivity.class.getSimpleName();
    private SlidingUpPanelLayout mLayout;
    private boolean isShuffle = false;
    private boolean isLoop = false;
    Handler mHandler = new Handler();
    FavouriteManager favouriteManager;
    List<SongDTO> songDTOList = new ArrayList<>();
    public static InterstitialAd mInterstitialAd;


    // Method to check whether the songs any song is being played or not
    private boolean songPresent() {
        if (txtSongDisplayName.getText().toString().equals(getString(R.string.no_song_selected))) {
            return false;
        }
        return true;
    }

    /*Setting up the connection with the service*/
    private ServiceConnection musicConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            MusicService.MusicBinder binder = (MusicService.MusicBinder) iBinder;
            musicService = binder.getService();
            musicBound = true;
            musicService.setServiceCallbacks(MusicHomeActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            musicBound = false;
        }
    };


    // Handling button click events using Butterknife

    // PLAY button
    @OnClick(R.id.btnPlay)
    void playMusic() {
        if (songPresent()) {
            musicService.go();
            updateProgressBar();
            btnPlay.setVisibility(View.GONE);
            btnPause.setVisibility(View.VISIBLE);
            btnPlayer.setImageResource(R.drawable.ic_action_pause_black);

        }
    }

    // PAUSE button
    @OnClick(R.id.btnPause)
    void pauseMusic() {
        if (songPresent()) {
            pause();
            btnPause.setVisibility(View.GONE);
            btnPlay.setVisibility(View.VISIBLE);
            btnPlayer.setImageResource(R.drawable.ic_action_name_black);
        }
    }

    // SHUFFLE button
    @OnClick(R.id.btnShuffle)
    void startShuffle() {
        if (songPresent()) {
            if (isShuffle) {
                isShuffle = false;
                btnShuffle.setImageResource(R.drawable.ic_action_shuffle_black);
                musicService.setShuffle();
            } else {
                btnShuffle.setImageResource(R.drawable.ic_action_shuffle_active);
                musicService.setShuffle();
                isShuffle = true;
            }
        }
    }

    // SONG LOOP button
    @OnClick(R.id.btnLoop)
    void loop() {
        if (songPresent()) {
            if (isLoop) {
                isLoop = false;
                btnLoop.setImageResource(R.drawable.ic_action_loop_black);
                musicService.setLoop();
            } else {
                isLoop = true;
                btnLoop.setImageResource(R.drawable.ic_action_loop_active);
                musicService.setLoop();
            }
        }
    }

    // NEXT SONG button
    @OnClick(R.id.btnNext)
    void next() {
        if (songPresent()) {
            /*if (checkFavourite(musicService.getCurrentTrack())) {
                btnFavourite.setImageResource(R.drawable.ic_action_star_black);
            } else {
                btnFavourite.setImageResource(R.drawable.ic_action_star_border_black);
            }*/
            playNextSong();
        }
    }

    // PREVIOUS SONG button
    @OnClick(R.id.btnPrevious)
    void prev() {
        if (songPresent()) {
            playPrev();
            if (checkFavourite(musicService.getCurrentTrack())) {
                btnFavourite.setImageResource(R.drawable.ic_action_star_black);
            } else {
                btnFavourite.setImageResource(R.drawable.ic_action_star_border_black);
            }

        }
    }

    // PLAY/PAUSE BUTTON in now playing screen
    @OnClick(R.id.btnPlayer)
    void player() {
        if (songPresent()) {
            if (musicService.isPlaying()) {
                pause();
                btnPlayer.setImageResource(R.drawable.ic_action_name_black);
            } else {
                musicService.go();
                btnPlayer.setImageResource(R.drawable.ic_action_pause_black);
            }
        }
    }

    // MARK AS FAVOURITE and UNFAVOURITE button
    @OnClick(R.id.btnFavourite)
    void fav() {
        if (songPresent()) {
            if (checkFavourite(musicService.getCurrentTrack())) {
                Log.i("Song:::", musicService.getCurrentTrack().toString());
                favouriteManager.removeFavourite(this, musicService.getCurrentTrack());
                Toast.makeText(this, "Yeah!!! The song does loose taste after some time.Unfavourited!", Toast.LENGTH_SHORT).show();
                btnFavourite.setImageResource(R.drawable.ic_action_star_border_black);
            } else {
                favouriteManager.addFavourite(this, musicService.getCurrentTrack());
                Toast.makeText(this, "Umm.. Like your taste in music.Favourited!!!", Toast.LENGTH_SHORT).show();
                btnFavourite.setImageResource(R.drawable.ic_action_star_black);
            }
        }
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_home);

        AdView mAdView = findViewById(R.id.adView);
        btnFeelingLucky = findViewById(R.id.btnFeelingLucky);
        final AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        AdView adView = new AdView(MusicHomeActivity.this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-3415371062508470/2364188551");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3415371062508470/1313070572");
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new AdListener(){
            @Override
            public void onAdClosed() {
                mInterstitialAd.loadAd(adRequest);
            }
        });
        /*Starting the music service*/
        playIntent = new Intent(this, MusicService.class);
        bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
        startService(playIntent);

        /*Method to handle events of activity*/
        populate();
    }

    private void populate() {
        /*Initialsing Butterknife library*/
        ButterKnife.bind(this);

        /*Setting up toolbar*/
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initCollapsingToolbar();

        favouriteManager = new FavouriteManager();

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf");
        txtSongDisplayName.setTypeface(typeface);

        /*Setting up the view for Now Playing screen using sliding panel*/
        nowPlayingLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.i(TAG, "onPanelStateChanged " + newState);
                switch (newState) {
                    /*Handling COLLAPSED VIEW*/
                    case COLLAPSED:
                        imgCompactDisc.setImageResource(R.drawable.compact_disc);
                        btnFavourite.setVisibility(View.GONE);
                        if (musicService.isPlaying()) {
                            btnPlay.setVisibility(View.GONE);
                            btnPause.setVisibility(View.VISIBLE);
                        } else {
                            btnPlay.setVisibility(View.VISIBLE);
                            btnPause.setVisibility(View.GONE);
                        }
                        break;
                    /*Handling EXPANDED VIEW*/
                    case EXPANDED:
                        imgCompactDisc.setImageResource(R.drawable.ic_action_close_black);
                        if (musicService.getCurrentTrack() == null) {
                            btnFavourite.setImageResource(R.drawable.ic_action_star_border_black);
                            btnFavourite.setVisibility(View.VISIBLE);
                            btnPlay.setVisibility(View.GONE);
                            btnPause.setVisibility(View.GONE);
                            btnFavourite.setEnabled(false);
                        } else if (checkFavourite(musicService.getCurrentTrack())) {
                            btnFavourite.setVisibility(View.VISIBLE);
                            btnFavourite.setImageResource(R.drawable.ic_action_star_black);
                            btnPlay.setVisibility(View.GONE);
                            btnPause.setVisibility(View.GONE);
                            btnFavourite.setEnabled(true);
                        } else {
                            btnFavourite.setImageResource(R.drawable.ic_action_star_border_black);
                            btnFavourite.setVisibility(View.VISIBLE);
                            btnPlay.setVisibility(View.GONE);
                            btnPause.setVisibility(View.GONE);
                            btnFavourite.setEnabled(true);
                        }
                        break;
                }

            }
        });

        /*When layout starts fading it collapses automatically*/
        nowPlayingLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nowPlayingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        /*Seekbar to track the progress of song*/
        songProgressBar.setOnSeekBarChangeListener(this);

        /*Setting up the Navigation Drawer and setting up click events*/
        navigation_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                /*Checking if previously any item was checked and making the item selected as checked*/
                if (previousMenuItem != null)
                    previousMenuItem.setChecked(false);
                item.setCheckable(true);
                item.setChecked(true);
                previousMenuItem = item;
                //drawer_layout.closeDrawers();

                /*Setting up the fragments on Navigation Drawer click events*/
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                switch (item.getItemId()) {
                    /*All Songs Fragment*/
                    case R.id.all_songs:
                        AllSongsFragment allSongsFragment = new AllSongsFragment();
                        transaction.replace(R.id.frame, allSongsFragment);
                        transaction.addToBackStack("All Songs");
                        transaction.commit();
                        collapsingToolbar.setTitle(getString(R.string.all_songs));
                        drawer_layout.closeDrawers();
                        return true;

                    /*Favourites Fragment*/
                    case R.id.favourites:
                        FavouritesFragment favouritesFragment = new FavouritesFragment();
                        transaction.replace(R.id.frame, favouritesFragment);
                        transaction.addToBackStack("All Songs");
                        transaction.commit();
                        collapsingToolbar.setTitle(getString(R.string.favourites));
                        drawer_layout.closeDrawers();
                        return true;

                    /*Starting the Settings Activity*/
                    case R.id.settings:
                        startActivity(new Intent(MusicHomeActivity.this, SettingsActivity.class));
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                drawer_layout.closeDrawers();
                            }
                        }, 500);
                        return true;

                    /*About App fragment*/
                    case R.id.about:
                        AboutAppFragment aboutAppFragment = new AboutAppFragment();
                        transaction.replace(R.id.frame, aboutAppFragment);
                        transaction.commit();
                        transaction.addToBackStack("All Songs");
                        collapsingToolbar.setTitle(getString(R.string.about_app));
                        drawer_layout.closeDrawers();
                        return true;

                    default:
                        return true;
                }
            }
        });

        /*By Default displaying All Songs Fragment*/
        AllSongsFragment fragment = new AllSongsFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        getSupportActionBar().setTitle(R.string.all_songs);
        fragmentTransaction.commit();

        /*Showing the Drawer Icon toggle effect on drawer open and close*/
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer_layout, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer_layout.setDrawerListener(actionBarDrawerToggle);

        /*Showing the three parallel lines as menu icon*/
        actionBarDrawerToggle.syncState();

        btnFeelingLucky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mInterstitialAd.isLoaded()) {
                    if (isPlaying()) {
                        pause();
                        btnPause.setVisibility(View.GONE);
                        btnPlay.setVisibility(View.VISIBLE);
                        btnPlayer.setImageResource(R.drawable.ic_action_name_black);

                    } else {
                        startActivityForResult(new Intent(MusicHomeActivity.this, FullscreenActivity.class), 311);
                    }
                    startActivityForResult(new Intent(MusicHomeActivity.this, FullscreenActivity.class), 131);

                } else {
                    Log.i("Ads:", "Not Loaded yet");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                drawer_layout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Fragment f = new Fragment();
        if (nowPlayingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            nowPlayingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
        /*Preventing to stop so that music runs in background*/
            moveTaskToBack(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(musicConnection);
        musicService.stopForeground(true);
        mHandler.removeCallbacks(updateTimeTask);
        android.os.Process.killProcess(Process.myPid());
    }

    /*Callback method from fragments so that activity has track which music item was clicked in the fragment*/
    @Override
    public void onArticleSelected(List<SongDTO> songDTOList, SongDTO dto) {
        this.songDTOList = songDTOList;
        musicService.setList(songDTOList);
        musicService.setSong(songDTOList.indexOf(dto));
        musicService.play();
        updateProgressBar();

        /*Using Glide library to load the song thumbnails*/
        Glide.with(this).load(ContentUris.withAppendedId(Uri.parse("content://media/external/audio/albumart"),
                dto.getAlbumID())).placeholder(R.drawable.musicplayer).error(R.drawable.musicplayer)
                .crossFade().centerCrop().into(imgThumbnail);
        txtSongDisplayName.setText(dto.getName());
        btnPlay.setVisibility(View.GONE);
        btnFavourite.setVisibility(View.GONE);
        btnPause.setVisibility(View.VISIBLE);
        btnPlayer.setImageResource(R.drawable.ic_action_pause_black);
        if (playbackPaused) {
            playbackPaused = false;
        }

    }

    /*Methods to handle the seek progress and tracking seek touch events*/
    public void updateProgressBar() {
        mHandler.postDelayed(updateTimeTask, 100);
    }

    /*Updating seek bar progress at every second*/
    private Runnable updateTimeTask = new Runnable() {
        @Override
        public void run() {
            long totalDuration = musicService.getDuration();
            long currentDuration = musicService.getPosition();
            Double percentage = (double) 0;

            long currentSeconds = (int) (currentDuration / 1000);
            long totalSeconds = (int) (totalDuration / 1000);

            // calculating percentage
            percentage = (((double) currentSeconds) / totalSeconds) * 100;
            songProgressBar.setProgress(percentage.intValue());
            mHandler.postDelayed(this, 100);
        }
    };


    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(updateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(updateTimeTask);
        int totalDuration = musicService.getDuration();
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double) songProgressBar.getProgress() / 100) * totalDuration) * 1000);

        // return current duration in milliseconds
        seekTo(currentDuration);
        updateProgressBar();
    }

    /*Method to play next song (interface method of music service)*/
    @Override
    public void playNextSong() {
        musicService.playNext();
        Glide.with(this).load(musicService.getSongImage()).placeholder(R.drawable.musicplayer).error(R.drawable.musicplayer)
                .crossFade().centerCrop().into(imgThumbnail);
        txtSongDisplayName.setText(musicService.getSongTitle());
        /*Checking whether song is in playlist or not*/
        if (checkFavourite(musicService.getCurrentTrack())) {
            btnFavourite.setImageResource(R.drawable.ic_action_star_black);
        } else {
            btnFavourite.setImageResource(R.drawable.ic_action_star_border_black);
        }
        if (playbackPaused) {
            playbackPaused = false;
        }
    }

    /*Method to play previous song*/
    public void playPrev() {
        musicService.playPrev();
        Glide.with(this).load(musicService.getSongImage()).placeholder(R.drawable.musicplayer).error(R.drawable.musicplayer)
                .crossFade().centerCrop().into(imgThumbnail);
        txtSongDisplayName.setText(musicService.getSongTitle());
        if (playbackPaused) {
            playbackPaused = false;
        }
    }

    @Override
    public void start() {
        musicService.go();
    }

    @Override
    public void pause() {
        playbackPaused = true;
        musicService.pausePlayer();
    }

    @Override
    public int getDuration() {
        if (musicService != null && musicBound && musicService.isPlaying())
            return musicService.getDuration();
        else
            return 0;
    }

    @Override
    public int getCurrentPosition() {
        if (musicService != null && musicBound && musicService.isPlaying())
            return musicService.getPosition();
        else
            return 0;
    }

    @Override
    public void seekTo(int i) {
        musicService.seek(i);
    }

    @Override
    public boolean isPlaying() {
        if (musicService != null && musicBound)
            return musicService.isPlaying();
        return false;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    /*Setting up the collapsing toolbar*/
    private void initCollapsingToolbar() {
        collapsingToolbar.setTitle("Music Home");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    //collapsingToolbar.setTitle(getString(R.string.all_songs));
                    isShow = true;
                } else if (isShow) {
                    //collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    /*Method to check whether the given prticular song is favourite or not*/
    private boolean checkFavourite(SongDTO currentTrack) {
        boolean isFav = false;
        List<SongDTO> favourites = favouriteManager.getFavourites(MusicHomeActivity.this);
        try {
            if (favourites != null) {
                for (SongDTO songDTO : favourites) {
                    if (songDTO.getData().equals(currentTrack.getData())) {
                        isFav = true;
                        break;
                    }
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return isFav;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == 131 || requestCode == 311) && resultCode == RESULT_OK) {

            //musicService.play();
        }
    }
}
