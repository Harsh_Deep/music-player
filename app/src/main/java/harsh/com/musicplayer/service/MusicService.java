package harsh.com.musicplayer.service;

/**
 * Created by Harsh on 9/7/2017.
 */

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import harsh.com.musicplayer.R;
import harsh.com.musicplayer.activity.MusicHomeActivity;
import harsh.com.musicplayer.dto.SongDTO;

/*Service for doing majority of things with the media player*/
public class MusicService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    public static MediaPlayer player;
    private List<SongDTO> songDTOList;
    private int position;
    private final IBinder musicBind = new MusicBinder();
    private String songTitle = "";
    private static final int NOTIFY_ID = 1;
    private boolean shuffle = false;
    private boolean loop = false;
    private Random random;
    final public static Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
    private String CHANNEL_ID = "music_player_harsh_01";

    /*Interface for communicating with the activity*/
    public interface ServiceCallbacks {
        void playNextSong();
    }

    private ServiceCallbacks serviceCallbacks;

    public void setServiceCallbacks(ServiceCallbacks callbacks) {
        serviceCallbacks = callbacks;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        position = 0;
        random = new Random();
        player = new MediaPlayer();
        init();
    }

    public class MusicBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        player.stop();
        player.release();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        if (player.getCurrentPosition() >= 0) {
            serviceCallbacks.playNextSong();
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        Log.v("MUSIC PLAYER", "Playback Error");
        mediaPlayer.reset();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
        createNotification();
    }


    public void createNotification() {
        Intent intent = new Intent(this, MusicHomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        CharSequence name = getString(R.string.app_name);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        /*Setting up the notification when music starts playing*/

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Notification.Builder builder = new Notification.Builder(this);

                builder.setContentIntent(pendingIntent)
                        .setSmallIcon(R.drawable.ic_stat_music)
                        .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.speaker))
                        .setOngoing(true)
                        .setChannelId(CHANNEL_ID)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(songTitle)
                        .setStyle(new Notification.MediaStyle());

                Notification notification = builder.build();
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
                notificationManager.createNotificationChannel(channel);
                startForeground(NOTIFY_ID, notification);


            } else {
                Notification.Builder builder = new Notification.Builder(this);

                builder.setContentIntent(pendingIntent)
                        .setSmallIcon(R.drawable.ic_stat_music)
                        .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.speaker))
                        .setOngoing(true)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(songTitle);

                Notification notification = builder.build();
                startForeground(NOTIFY_ID, notification);

            }
        }
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
    }

    /*Initialising the music player*/
    public void init() {
        player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        player.setOnPreparedListener(this);
        player.setOnErrorListener(this);
        player.setOnCompletionListener(this);
    }

    /*Setting the song list*/
    public void setList(List<SongDTO> songDTOList) {
        this.songDTOList = songDTOList;
    }

    /*Method to play the songs*/
    public void play() {
        player.reset();
        try {
            SongDTO dto = songDTOList.get(position);
            songTitle = dto.getName();

            try {
                player.setDataSource(dto.getData());
                player.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (NullPointerException e) {
            Toast.makeText(getApplicationContext(), "Oh Come On! Atleast Pick One!!!", Toast.LENGTH_SHORT).show();
        }
    }


    public void setSong(int index) {
        position = index;
    }

    /*Method to play next*/
    public void playNext() {
        try {
            if (loop) {
                play();
                return;
            }
            if (shuffle) {
                int newSong = position;
                while (newSong == position) {
                    newSong = random.nextInt(songDTOList.size());
                }
                position = newSong;
                play();
                return;
            }
            position++;
            if (position >= songDTOList.size())
                position = 0;
            play();

        } catch (NullPointerException e) {
            Toast.makeText(getApplicationContext(), "Oh Come On! Atleast Pick One!!!", Toast.LENGTH_SHORT).show();
        }

    }

    /*Playing previous song*/
    public void playPrev() {
        if (loop) {
            play();
            return;
        }
        position--;
        if (position < 0)
            position = songDTOList.size() - 1;
        play();
    }

    /*Method to pause the music player*/
    public void pausePlayer() {
        player.pause();
    }

    /*Starting music*/
    public void go() {
        player.start();
    }

    /*Method to get the currently playing track information*/
    public SongDTO getCurrentTrack() {
        try {
            if (songDTOList.size() != 0) {
                return songDTOList.get(position);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*Get position of the track in the playlist*/
    public int getPosition() {
        return player.getCurrentPosition();
    }

    /*Get complete duration of the song*/
    public int getDuration() {
        return player.getDuration();
    }

    /*Check whether music player is playing or not*/
    public boolean isPlaying() {
        return player.isPlaying();
    }

    /*Getting the position of the seek bar*/
    public void seek(int posn) {
        player.seekTo(posn);
    }

    /*Getting the title of the song being played*/
    public String getSongTitle() {
        try {
            if (songDTOList.size() != 0) {
                return songDTOList.get(position).getName();
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return "You Got Nothing!!!";
    }

    /*Getting the ALBUM_ART of the song being played*/
    public Uri getSongImage() {
        try {
            if (songDTOList.size() != 0) {
                Uri uri = ContentUris.withAppendedId(sArtworkUri, songDTOList.get(position).getAlbumID());
                return uri;
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    /*To check whether shuffle is on or not*/
    public void setShuffle() {
        if (shuffle)
            shuffle = false;
        else
            shuffle = true;
    }

    /*To check whether user wants to play the song in loop or not*/
    public void setLoop() {
        if (loop)
            loop = false;
        else
            loop = true;
    }
}
