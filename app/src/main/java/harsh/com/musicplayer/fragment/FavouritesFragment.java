package harsh.com.musicplayer.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import harsh.com.musicplayer.R;
import harsh.com.musicplayer.activity.MusicHomeActivity;
import harsh.com.musicplayer.adapter.AllSongsAdapter;
import harsh.com.musicplayer.dto.SongDTO;
import harsh.com.musicplayer.service.MusicService;
import harsh.com.musicplayer.util.FavouriteManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavouritesFragment extends Fragment {

    List<SongDTO> favList;
    FavouriteManager favouriteManager;
    private View parentView;
    private AllSongsAdapter adapter;
    @Bind(R.id.listFavSongs)
    RecyclerView listFavSongs;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public FavouritesFragment() {
        // Required empty public constructor
    }

    AllSongsFragment.OnHeadlineSelectedListener mCallback;

    public interface OnHeadlineSelectedListener {
        void onArticleSelected(List<SongDTO> songDTOList, SongDTO songDTO);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mCallback = (AllSongsFragment.OnHeadlineSelectedListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_favourites, container, false);
        ButterKnife.bind(this, parentView);
        populate();
        return parentView;
    }

    private void populate() {
        favouriteManager = new FavouriteManager();
        favList = favouriteManager.getFavourites(getActivity());
        if (favList == null) {
            Toast.makeText(getActivity(), "No Favourite Songs!!!", Toast.LENGTH_SHORT).show();
        } else {
            if (favList.size() == 0) {
                Toast.makeText(getActivity(), "No Favourite Songs!!!", Toast.LENGTH_SHORT).show();
            }

            if (favList != null) {
                adapter = new AllSongsAdapter(getActivity(), favList, new AllSongsAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(SongDTO songDTO) {
                        if ((MusicService.player != null)
                                && new MusicService().isPlaying()
                                && (MusicHomeActivity.musicService.getSongTitle().equals(songDTO.name))) {
                            SlidingUpPanelLayout panel = getActivity().findViewById(R.id.sliding_layout);
                            panel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                        } else {
                            mCallback.onArticleSelected(favList, songDTO);
                        }
                    }
                });
                listFavSongs.setAdapter(adapter);
                listFavSongs.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            }
        }
    }
}
