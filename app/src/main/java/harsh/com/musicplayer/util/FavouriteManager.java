package harsh.com.musicplayer.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import harsh.com.musicplayer.dto.SongDTO;

/**
 * Created by Harsh on 9/8/2017.
 */

/*Util class to save the favourite songs of the user*/
public class FavouriteManager {

    public static final String PREFS_NAME = "MUSIC_PLAYER";
    public static final String FAVORITES = "Favourite_Music";

    public FavouriteManager() {
        super();
    }

    /*Method to save the favourite songs as json in shared preferences*/
    public void saveFavourite(Context context, List<SongDTO> songDTOList) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(songDTOList);
        editor.putString(FAVORITES, jsonFavorites);
        editor.commit();
    }

    /*Method to add song to the favourite list*/
    public void addFavourite(Context context, SongDTO dto) {
        List<SongDTO> favourites = getFavourites(context);
        if (favourites == null)
            favourites = new ArrayList<>();
        favourites.add(dto);
        saveFavourite(context, favourites);
    }

    /*Method to remove the songs from favourite list*/
    public void removeFavourite(Context context, SongDTO dto) {
        ArrayList<SongDTO> favourites = getFavourites(context);
        Iterator<SongDTO> iterator = favourites.iterator();
        String title = "";
        while (iterator.hasNext()) {
            SongDTO dto1 = iterator.next();
            title = dto1.getData();
            if (title.equals(dto.getData())) {
                iterator.remove();
                saveFavourite(context, favourites);
            }
        }
    }

    /*Method to fetch the favourite list*/
    public ArrayList<SongDTO> getFavourites(Context context) {
        SharedPreferences settings;
        List<SongDTO> songDTOList;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavourites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            SongDTO[] favSongs = gson.fromJson(jsonFavourites, SongDTO[].class);

            songDTOList = Arrays.asList(favSongs);
            songDTOList = new ArrayList<SongDTO>(songDTOList);
        } else {
            return null;
        }
        return (ArrayList<SongDTO>) songDTOList;
    }
}
