package harsh.com.musicplayer.fragment;


import android.content.Context;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import harsh.com.musicplayer.R;
import harsh.com.musicplayer.activity.MusicHomeActivity;
import harsh.com.musicplayer.adapter.AllSongsAdapter;
import harsh.com.musicplayer.dto.SongDTO;
import harsh.com.musicplayer.service.MusicService;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllSongsFragment extends Fragment {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private View parentView;
    @Bind(R.id.listAllSongs)
    RecyclerView listAllSongs;
    private AllSongsAdapter adapter;
    private List<SongDTO> songDTOList;
    MediaPlayer mediaPlayer;

    public AllSongsFragment() {
        // Required empty public constructor
    }

    /*Interface for sending the data to main activity about the click events*/
    public interface OnHeadlineSelectedListener {
        void onArticleSelected(List<SongDTO> songDTOList, SongDTO songDTO);
    }

    OnHeadlineSelectedListener mCallback;

    /*Attaching it to the activity*/
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mCallback = (OnHeadlineSelectedListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_all_songs, container, false);
        ButterKnife.bind(this, parentView);
        populate();
        return parentView;
    }

    private void populate() {
        setHasOptionsMenu(true);

        songDTOList = new ArrayList<>();
        mediaPlayer = new MediaPlayer();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!=0";
        Cursor cursor = getActivity().getContentResolver().query(uri, null, selection, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    String name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME));
                    String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                    long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ID));
                    SongDTO dto = new SongDTO();
                    dto.setName(name);
                    dto.setArtist(artist);
                    dto.setData(data);
                    dto.setAlbumID(id);
                    songDTOList.add(dto);
                } while (cursor.moveToNext());
            }
            cursor.close();
            adapter = new AllSongsAdapter(getActivity(), songDTOList, new AllSongsAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(SongDTO songDTO) {
                    if ((MusicService.player != null)
                            && new MusicService().isPlaying()
                            && (MusicHomeActivity.musicService.getSongTitle().equals(songDTO.name))) {
                        SlidingUpPanelLayout panel = getActivity().findViewById(R.id.sliding_layout);
                        panel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                    } else {
                        mCallback.onArticleSelected(songDTOList, songDTO);
                    }
                }
            });
            listAllSongs.setAdapter(adapter);
            listAllSongs.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            /*Performing sorting of songs by name*/
            case R.id.sort_by_name:
                Collections.sort(songDTOList, new Comparator<SongDTO>() {
                    @Override
                    public int compare(SongDTO songDTO, SongDTO t1) {
                        return songDTO.name.compareToIgnoreCase(t1.name);
                    }
                });
                adapter.notifyDataSetChanged();
        }
        return super.onOptionsItemSelected(item);
    }
}
