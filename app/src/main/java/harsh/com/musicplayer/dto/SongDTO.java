package harsh.com.musicplayer.dto;

/**
 * Created by Harsh on 9/6/2017.
 */

/*Model class for all songs
* The data of all songs will be stored in the for of this object
*/

public class SongDTO {
    public long albumID;
    public String name;
    public String artist;
    public String data;

    public long getAlbumID() {
        return albumID;
    }

    public void setAlbumID(long albumID) {
        this.albumID = albumID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
