package harsh.com.musicplayer.adapter;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import harsh.com.musicplayer.R;
import harsh.com.musicplayer.dto.SongDTO;

/**
 * Created by Harsh on 9/6/2017.
 */

public class AllSongsAdapter extends RecyclerView.Adapter<AllSongsAdapter.AllSongsViewHolder> {

    private LayoutInflater inflater;
    private Context context;
    private List<SongDTO> songDTOList;
    final public static Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");

    /*Interface to handle click events of the Recycler view*/
    public interface OnItemClickListener {
        void onItemClick(SongDTO songDTO);
    }

    private OnItemClickListener listener;

    public AllSongsAdapter(Context context, List<SongDTO> songDTOList, OnItemClickListener listener) {
        this.context = context;
        this.songDTOList = songDTOList;
        this.listener = listener;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public AllSongsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*Creating the view of single row*/
        View view = inflater.inflate(R.layout.songs_custom_row, parent, false);
        AllSongsViewHolder holder = new AllSongsViewHolder(view);
        return holder;
    }

    /*Binding view when recycled*/
    @Override
    public void onBindViewHolder(AllSongsViewHolder holder, int position) {
        holder.bind(songDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return songDTOList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /*View holder class sets the content of the views inflated in recycler view*/
    class AllSongsViewHolder extends RecyclerView.ViewHolder {
        TextView txtNameOfSong, txtSongArtist;
        ImageView imgListThumbnail;

        public AllSongsViewHolder(View itemView) {
            super(itemView);
            imgListThumbnail = itemView.findViewById(R.id.imgListThumbnail);
            txtNameOfSong = itemView.findViewById(R.id.txtNameOfSong);
            txtSongArtist = itemView.findViewById(R.id.txtSongArtist);
        }

        public void bind(final SongDTO songDTO, final OnItemClickListener listener) {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Whitney-Book-Bas.otf");
            Uri uri = ContentUris.withAppendedId(sArtworkUri,
                    songDTOList.get(getPosition()).getAlbumID());
            Glide.with(context).load(uri).placeholder(R.drawable.musicplayer).error(R.drawable.musicplayer)
                    .crossFade().centerCrop().into(imgListThumbnail);
            txtNameOfSong.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Whitney-Semibold-Bas.otf"));
            txtSongArtist.setTypeface(typeface);
            txtNameOfSong.setText(songDTO.getName());
            txtSongArtist.setText(songDTO.getArtist());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(songDTO);
                }
            });
        }
    }
}
